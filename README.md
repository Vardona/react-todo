In this project I have used few packages such as: 
-	React Redux for state management 
-	React Bootstrap for styling 


Inside the src folder you can find: 

-	Setup folder, which includes the initial store and reducers, also model for validations 
-	Styles, which include CSS files 
-	Todo folder, which includes the Todo Model, actions, reducers and selector
-	View folder, which includes all the Components that are used in this app

My main focus was on the functionality of the App and did not put much effort on styling.😊


