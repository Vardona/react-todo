import TodoReducers from '../todo/todoReducers.js';

const rootReducer = (state, action) => {
    return TodoReducers(state, action);
};

export default rootReducer;