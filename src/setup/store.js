import Reducers from './reducers';
import { createStore } from 'redux';

export default function configureStore() {
    const store = createStore(Reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

    return store;
}
