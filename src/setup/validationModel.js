import { List } from 'immutable';

export default class ValidationModel {
    constructor() {
        this.isValid = true;
        this.validationMessages = List();
    }
}

