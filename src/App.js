import React from 'react';
import configureStore from './setup/store';
import { Provider } from 'react-redux';
import MainComponent from './views/common/mainComponent';

const store = configureStore();

function App() {
    return (
        <div className="App">
            <Provider store={store}>
                <MainComponent />
            </Provider>
        </div>
    );
}

export default App;
