import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import '../../styles/Sidebar.css';
import PropTypes from 'prop-types';
import TodoSelectors from '../../todo/todoSelectors';
import TodoActionCreators from '../../todo/todoActionCreators';

const mapStoreToProps = (store ) => {
    return { showTodoModal: TodoSelectors.showModal(store) };
};

const mapDispatchToProps = dispatch => {
    return { changeModalState: showModal => dispatch(TodoActionCreators.changeModalState(showModal)) };
};

class Sidebar extends Component {
    static get propTypes() {
        return {
            showTodoModal: PropTypes.bool,
            changeModalState: PropTypes.func
        };
    }
	
	_onBtnClick = () => {
	    this.props.changeModalState(true);
	}

	render() {
	    return (
	        <div className="sidenav">
	            <h3 className="mx-5">Todo React App</h3>
	            <Button className="ml-2" variant="light" onClick={() => this._onBtnClick()}>Add new todo</Button>
	        </div>
	    );
	}
}

export default connect(
    mapStoreToProps,
    mapDispatchToProps)(Sidebar);