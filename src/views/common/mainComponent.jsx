import React, { Component } from 'react';
import TodoComponent from '../workflow/todoComponent.jsx';
import Sidebar from './sidebar.jsx';

export default class MainComponent extends Component {
    render() {
        return (
            <>
                <Sidebar />
                <TodoComponent />
            </>
        );
    }
}