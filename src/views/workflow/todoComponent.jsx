import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TodoSelectors from '../../todo/todoSelectors';
import TodoActionCreators from '../../todo/todoActionCreators';
import ModalTodo from './forms/todo/modalTodo';
import ValidationModel from '../../setup/validationModel.js';
import Todo from '../../todo/todo';
import validateTodoForm from './forms/todo/addTodoValidation.js';
import TodoList from './forms/todo/todoList';
import { List } from 'immutable';
import TodoSearchFilter from './forms/todo/todoSearchFilter';

const mapStoreToProps = (store ) => {
    return {
        showTodoModal: TodoSelectors.showModal(store),
        todo: TodoSelectors.getTodo(store),
        todos: TodoSelectors.getTodos(store)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        changeModalState: showModal => dispatch(TodoActionCreators.changeModalState(showModal)),
        addTodo: todo => dispatch(TodoActionCreators.addTodo(todo)),
        editTodo: todo => dispatch(TodoActionCreators.editTodo(todo)),
        getTodos: filter => dispatch(TodoActionCreators.getTodos(filter))
    };
};

class TodoComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todo: new Todo(),
            todos: List(),
            validationModel: new ValidationModel(),
            isUpdating: false,
            readOnly: false,
            sortDirection: {
                'dueDate': undefined,
                'title': undefined
            },
            filter: ''
        };
    }

    componentDidMount() {
        const { getTodos } = this.props;
        getTodos({});
    }

    componentDidUpdate(prevProps) {
        const { todos } = this.props;
        if(prevProps.todos !== todos ) {
            this.setState({ todos: todos.sort((a, b) => a.title.localeCompare((b.title))) });
        }
    }

    static get propTypes() {
        return {
            showTodoModal: PropTypes.bool,
            todos: PropTypes.instanceOf(List),
            changeModalState: PropTypes.func,
            addTodo: PropTypes.func,
            editTodo: PropTypes.func,
            getTodos: PropTypes.func
        };
    }

    _onComplete = () => {
        const { addTodo, changeModalState, editTodo } = this.props;
        const { todo, isUpdating } = this.state;
        const validationModel = validateTodoForm(todo);
        this.setState({ validationModel });
        if(validationModel.isValid) {
            if(isUpdating) {
                editTodo(todo);
            }
            else {
                addTodo(todo);
            }
            changeModalState(false);
            this._onHide();
        }
    }

    _onHide = () => {
        const { changeModalState } = this.props;
        this.setState({
            todo: new Todo(),
            validationModel: new ValidationModel(),
            readOnly: false,
            isUpdating: false
        });
        changeModalState(false);
    }

    _onTodoClick = (todo, showTodoModal, readOnly) => {
        const { changeModalState } = this.props;
        this.setState({
            todo,
            readOnly
        });
        changeModalState(showTodoModal);
    }

    _changeIsUpdatingState =(todo, showTodoModal, isUpdating) => {
        const { changeModalState } = this.props;
        this.setState({
            todo,
            isUpdating
        });
        changeModalState(showTodoModal);
    }

    _sortBy = (key, updateDirection = true) => {
        const { todos, sortDirection } = this.state;
        this.setState({
            todos: key === 'dueDate' ?
                sortDirection[key] === 'asc' ? todos.sort((a, b) => a[key].diff(b[key])) : todos.sort((a, b) => b[key].diff(a[key])) :
                sortDirection[key] === 'asc' ? todos.sort((a, b) => a[key].localeCompare((b[key]))) : todos.sort((a, b) => b[key].localeCompare((a[key]))),
            sortDirection: updateDirection ? { [key]: sortDirection[key] === 'asc' ? 'desc' : 'asc' } : sortDirection
        });
    }

    _onFilterChange = () => {
        const { todos, filter } = this.state;
        this.setState({ todos: filter ?
            todos.filter(todo => todo.title.includes(filter)).sort((a, b) => a.title.localeCompare((b.title))) :
            this.props.todos.sort((a, b) => a.title.localeCompare((b.title))) });

    }

    render() {
        const { showTodoModal } = this.props;
        const { validationModel, todo, readOnly, isUpdating, todos, sortDirection, filter } = this.state;
        const modalProps = {
            todo,
            validationModel,
            onPropertyChange: (todo) => this.setState({ todo }),
            onHide: this._onHide,
            onSave: this._onComplete,
            readOnly,
            isUpdating
        };

        const listProps = {
            todos,
            onItemClick: this._onTodoClick,
            changeIsUpdatingState: this._changeIsUpdatingState,
            sortBy: this._sortBy,
            sortDirection
        };

        const filterProps = {
            onFilterChange: (filter) => this.setState({ filter }, () => this._onFilterChange()),
            filter
        };

        return (
            <>
                {showTodoModal && <ModalTodo {...modalProps} />}
                <TodoSearchFilter {...filterProps} />
                <TodoList {...listProps} />
            </>
        );
    }
}

export default connect(mapStoreToProps, mapDispatchToProps)(TodoComponent);