import { List } from 'immutable';
import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import Todo from './todo.jsx';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortDown } from '@fortawesome/free-solid-svg-icons/faSortDown';
import { faSortUp } from '@fortawesome/free-solid-svg-icons/faSortUp';

import '../../../../styles/TodoList.css';

export default class TodoList extends Component {
    static get propTypes() {
        return {
            todos: PropTypes.instanceOf(List),
            onItemClick: PropTypes.func,
            changeIsUpdatingState: PropTypes.func,
            sortBy: PropTypes.func,
            sortDirection: PropTypes.object
        };
    }

    render() {
        const { todos, onItemClick, changeIsUpdatingState, sortBy, sortDirection } = this.props;
        
        return (
            <Table striped bordered hover className="listMargin">
                <thead>
                    <tr>
                        <th onClick={() => sortBy('title')}>
                            Title
                            <FontAwesomeIcon icon={sortDirection.title === 'asc' ? faSortDown : sortDirection.title === 'desc' ? faSortUp : undefined} />
                        </th>
                        <th onClick={() => sortBy('dueDate')}>
                            Due Date
                            <FontAwesomeIcon icon={sortDirection.dueDate === 'asc' ? faSortDown : sortDirection.dueDate === 'desc' ? faSortUp : undefined} />
                        </th>
                        <th>Completed</th>
                    </tr>
                </thead>
                <tbody>
                    {todos.map((todo) => (
                        <Todo key={todo.id} todo={todo} onItemClick={onItemClick} changeIsUpdatingState={changeIsUpdatingState} />
                    ))}
	
                </tbody>
            </Table>
        );
    }
}