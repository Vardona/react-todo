import React, { Component } from 'react';
import '../../../../styles/TodoList.css';
import PropTypes from 'prop-types';

export default class TodoSearchFilter extends Component {
    static get propTypes() {
        return {
            onFilterChange: PropTypes.func,
            filter: PropTypes.func
        };
    }

    render() {
        const { onFilterChange, filter } = this.props;
        
        return (
            <input
                className="form-control listMargin"
                id="filter" type="text"
                placeholder="Search for todos here"
                onChange={(e) => onFilterChange(e.target.value)}
                value={filter}
            />
        );
    }
}