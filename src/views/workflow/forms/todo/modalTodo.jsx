import React, { Component } from 'react';
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import ValidationSummary from '../../../common/validationSummary';
import PropTypes from 'prop-types';
import Todo from '../../../../todo/todo';
import ReactDatePicker from 'react-datepicker';
import ValidationModel from '../../../../setup/validationModel';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

const ATTRIBUTES = {
    TITLE: 'title',
    DESCRIPTION: 'description',
    DUE_DATE: 'dueDate',
    COMPLETED: 'isCompleted'
};

class ModalTodo extends Component {
    static get proptTypes() {
        return {
            onHide: PropTypes.func,
            onPropertyChange: PropTypes.func,
            onSave: PropTypes.func,
            readOnly: PropTypes.bool,
            isUpdating: PropTypes.bool,
            todo: PropTypes.instanceOf(Todo),
            validationModel: PropTypes.instanceOf(ValidationModel)
        };
    }

     _onPropertyChange = (propertyName, propertyValue) => {
         const { todo, onPropertyChange } = this.props;
         const newTodo = { ...todo };
         newTodo[propertyName] = propertyValue;
         onPropertyChange(newTodo);
     };

     render() {
         const { onHide, onSave, todo, readOnly, validationModel, isUpdating } = this.props;

         return (
             <Modal show onHide={onHide} size="lg" centered>
                 <Modal.Header closeButton>
                     <Modal.Title>
                         {readOnly ? 'View Todo' : isUpdating ? 'Edit Todo' : 'Add new Todo'}
                     </Modal.Title>
                 </Modal.Header>
                 <Modal.Body>
                     <ValidationSummary validationModel={validationModel} />
                     <Form>
                         <Row className="extra-space-below mb-2">
                             <Col sm={3}>Title</Col>
                             <Col >
                                 <Form.Control
                                     onChange={e => this._onPropertyChange(ATTRIBUTES.TITLE, e.target.value)}
                                     value={todo[ATTRIBUTES.TITLE]}
                                     readOnly={readOnly}
                                     plaintext={readOnly}
                                 />
                             </Col>
                         </Row>
                         <Row className="extra-space-below mb-2">
                             <Col sm={3}>Description</Col>
                             <Col>
                                 <Form.Control
                                     onChange={e => this._onPropertyChange(ATTRIBUTES.DESCRIPTION, e.target.value)}
                                     value={todo[ATTRIBUTES.DESCRIPTION]}
                                     as="textarea"
                                     rows="4"
                                     readOnly={readOnly}
                                     plaintext={readOnly}
                                 />
                             </Col>
                         </Row>
                         <Row className="extra-space-below mb-2">
                             <Col sm={3}>Duedate</Col>
                             <Col>
                                 <ReactDatePicker
                                     showTimeSelect
                                     onChange={date => this._onPropertyChange(ATTRIBUTES.DUE_DATE, moment(date))}
                                     selected={todo[ATTRIBUTES.DUE_DATE] ? todo[ATTRIBUTES.DUE_DATE].toDate() : todo[ATTRIBUTES.DUE_DATE]}
                                     readOnly={readOnly}
                                 />
                             </Col>
                         </Row>
                         {readOnly &&
                    <Row className="extra-space-below mb-2">
                        <Col>
                            <p> {todo[ATTRIBUTES.COMPLETED] ? 'This todo is completed' : 'Todo is not completed yet'} </p>
                        </Col>
                    </Row>
                         }
                     </Form>
                 </Modal.Body>
                 <Modal.Footer>
                     <Button onClick={onHide} variant="default">
                    Cancel
                     </Button>
                     <Button onClick={readOnly ? onHide : e => onSave(e)} variant="primary">
                         {readOnly ? 'Done' : isUpdating ? 'Edit' : 'Add'}
                     </Button>
                 </Modal.Footer>
             </Modal>
         );
     }
}

export default ModalTodo;