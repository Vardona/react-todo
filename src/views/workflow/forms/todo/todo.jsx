import React, { Component } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import moment from 'moment';
import TodoActionCreators from '../../../../todo/todoActionCreators.js';
import { connect } from 'react-redux';

const ATTRIBUTES = { 
	COMPLETED: 'isCompleted', 
	DUE_DATE: 'dueDate',
	TITLE: 'title'
 };

const mapDispatchToProps = dispatch => {
    return {
        editTodo: todo => dispatch(TodoActionCreators.editTodo(todo)),
        removeTodo: todo => dispatch(TodoActionCreators.removeTodo(todo))
    };
};

class Todo extends Component {
    static get propTypes() {
        return {
            todo: PropTypes.object,
            onItemClick: PropTypes.func,
            editTodo: PropTypes.func,
            removeTodo: PropTypes.func,
            changeIsUpdatingState: PropTypes.func
        };
    }

	_onPropertyChange = (propertyName, propertyValue) => {
		const { todo, editTodo } = this.props;
		const newTodo = { ...todo };
		newTodo[propertyName] = propertyValue;
		editTodo(newTodo);
	};

	render() {
	    const { todo, onItemClick, changeIsUpdatingState, removeTodo } = this.props;

	    return (
	        <tr className={todo[ATTRIBUTES.COMPLETED] ? 'table-success' : moment(todo[ATTRIBUTES.DUE_DATE]).isBefore(moment()) ? 'table-danger' : ''} onClick={() => onItemClick(todo, true, true )}>
	            <td>{todo[ATTRIBUTES.TITLE]}</td>
	            <td>{moment(todo[ATTRIBUTES.DUE_DATE]).format('dddd, MMMM Do , h:mm a')}</td>
	            <td>
	                <Form.Check checked={todo[ATTRIBUTES.COMPLETED]} onClick={(e) => {
	                    e.stopPropagation();
	                    this._onPropertyChange(ATTRIBUTES.COMPLETED, e.target.checked);
	                }}
	                />
	            </td>
	            <td className="d-flex justify-content-between">
	                <Row>
	                    <Col>
	                        <Button variant="warning" onClick={e => {
	                            e.stopPropagation();
	                            changeIsUpdatingState(todo, true, true);
	                        }}
	                        > Edit</Button>
	                    </Col>
	                    <Col>
	                        <Button variant="danger" onClick={e => {
	                            e.stopPropagation();
	                            removeTodo(todo);
	                        }}
	                        > Remove</Button>
	                    </Col>
	                </Row>
	            </td>
	        </tr>
	    );
	}
}

export default connect(null, mapDispatchToProps)(Todo);