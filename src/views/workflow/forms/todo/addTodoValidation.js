import ValidationModel from '../../../../setup/validationModel';

const ATTRIBUTES = {
    TITLE: 'title',
    DESCRIPTION: 'description',
    DUE_DATE: 'dueDate'
};

const isNullOrWhitespace = (input) => {
    if(typeof input === 'undefined' || input === null) {
        return true;
    }
    if(typeof input !== 'string') {
        return false;
    }

    return input.replace(/\s/g, '').length < 1;
};

const validateTodoForm = (todo) => {
    const validationModel = new ValidationModel();
    if(isNullOrWhitespace(todo[ATTRIBUTES.TITLE])) {
        validationModel.validationMessages = validationModel.validationMessages.push('Title is required');
        validationModel.isValid = false;
    }
	
    return validationModel;
};

export default validateTodoForm;