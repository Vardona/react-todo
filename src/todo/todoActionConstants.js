import keyMirror from 'keymirror';

export default keyMirror({
    ADD_TODO: null,
    EDIT_TODO: null,
    GET_TODO: null,
    GET_TODOS: null,
    REMOVE_TODO: null,
    CHANGE_MODAL_STATE: null
});