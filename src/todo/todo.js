import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

export default class Todo {
    constructor(todo = {}) {
        const { title, description, dueDate, isCompleted } = todo;
        
        this.id = uuidv4();
        this.title = title || '';
        this.description = description || '';
        this.dueDate = dueDate ? moment(dueDate) : null;
        this.isCompleted = isCompleted || false;
    }

    static mapFromList = todos => {
        if(!todos) {
            return [];
        }

        return todos.map(todo => new Todo(todo));
    }
}