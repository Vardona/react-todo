
const todoSelectors = {
    getTodo: store => store.get('todo'),
    getTodos: store => store.get('todos'),
    showModal: store => store.get('showModal')
};

export default todoSelectors;

