import TodoActionConstants from './todoActionConstants.js';

const addTodo = todo => ({
    type: TodoActionConstants.ADD_TODO,
    payload: todo
});

const editTodo = todo => ({
    type: TodoActionConstants.EDIT_TODO,
    payload: todo
});

const removeTodo = todo => ({
    type: TodoActionConstants.REMOVE_TODO,
    payload: todo
});

const getTodo = todo => ({
    type: TodoActionConstants.GET_TODO,
    payload: todo
});

const getTodos = filter => ({
    type: TodoActionConstants.GET_TODOS,
    payload: filter
});

const changeModalState = showModal => ({
    type: TodoActionConstants.CHANGE_MODAL_STATE,
    payload: showModal
});

export default {
    addTodo,
    editTodo,
    removeTodo,
    getTodo,
    getTodos,
    changeModalState
};