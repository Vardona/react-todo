import { List, Map } from 'immutable';
import Todo from './todo';
import TodoActionConstants from './todoActionConstants';

const initialState = Map({
    todo: new Todo(),
    todos: List(),
    showModal: false
});

const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case TodoActionConstants.ADD_TODO: {
            localStorage.setItem('todo', JSON.stringify(action.payload));
            localStorage.setItem('todos', JSON.stringify(state.get('todos').push(action.payload)));
            
            return state.withMutations(state => {
                state.set('todo', new Todo(JSON.parse(localStorage.getItem('todo'))));
                state.set('todos', Todo.mapFromList(List(JSON.parse(localStorage.getItem('todos')))));
            });
        }
		
        case TodoActionConstants.EDIT_TODO: {
            const index = state.get('todos').findIndex(i => i.id === action.payload.id);
            localStorage.setItem('todo', JSON.stringify(action.payload));
            localStorage.setItem('todos', JSON.stringify(state.get('todos').update(index , () => action.payload)));

            return state.withMutations(state => {
                state.set('todo', new Todo(JSON.parse(localStorage.getItem('todo'))));
                state.set('todos', Todo.mapFromList(List(JSON.parse(localStorage.getItem('todos')))));
            });
        }
		
        case TodoActionConstants.REMOVE_TODO: {
            const index = state.get('todos').findIndex(i => i.id === action.payload.id);
            localStorage.setItem('todo', JSON.stringify(action.payload));
            localStorage.setItem('todos', JSON.stringify(state.get('todos').delete(index)));
            
            return state.withMutations(state => {
                state.set('todo', new Todo(JSON.parse(localStorage.getItem('todo'))));
                state.set('todos', Todo.mapFromList(List(JSON.parse(localStorage.getItem('todos')))));
            });
        }
		
        case TodoActionConstants.GET_TODO: {
            return state.set('todo', new Todo(action.payload));
        }

        case TodoActionConstants.GET_TODOS: {
            return state.set('todos', Todo.mapFromList(List(JSON.parse(localStorage.getItem('todos')))));
        }
		
        case TodoActionConstants.CHANGE_MODAL_STATE: {
            return state.set('showModal', action.payload);
        }
		
        default: {
            return state;
        }
    }
};

export default todoReducer;